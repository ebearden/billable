//
//  CSVExporter.swift
//  Billable
//
//  Created by Elvin Bearden on 6/11/16.
//  Copyright © 2016 Elvin Bearden. All rights reserved.
//

import Foundation

struct CSVExporter {
    static func export(_ client: Client) -> URL {
        let csvString = CSVExporter.csvString(client)
        
        let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
        let documents = paths.first ?? ""
        let filename = "/\(client.displayName)\(DateHelper.dateString(Date())).csv"
        let filepath = documents + filename
        let fileURL = URL(fileURLWithPath: filepath)
        
        do {
            try csvString.write(toFile: filepath, atomically: true, encoding: String.Encoding.utf8)
            return fileURL
        }
        catch let error {
            print(error)
            return URL(string: "")!
        }
    }
    
    static func csvString(_ client: Client) -> String {
        var csvString = "\(client.displayName) Billable Hours\n"
        csvString += ("Date,Hours,Minutes,Seconds,Total,Title")
        
        
        let dateEntries = client.dateEntries.map { (dateEntry) -> String in
            let billableEntries = dateEntry.billableEntries.map { (billableEntry) -> String in
                var billable = ""
                billable += "\n\(DateHelper.dateString(dateEntry.date)),"
                billable += "\(billableEntry.hours),"
                billable += "\(billableEntry.minutes),"
                billable += "\(billableEntry.seconds),"
                billable += "\(billableEntry.totalTime),"
                billable += "\(billableEntry.title ?? "")"
                
                return billable
            }
            
            return billableEntries.joined(separator: ",")
        }
        
        csvString += dateEntries.joined(separator: ",")
        
        return csvString
    }
}
