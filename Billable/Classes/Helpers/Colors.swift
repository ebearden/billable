//
//  Colors.swift
//  Billable
//
//  Created by Elvin Bearden on 6/25/16.
//  Copyright © 2016 Elvin Bearden. All rights reserved.
//

import UIKit

struct Colors {
    struct Base {
        static let green = UIColor(red:0.09, green:0.70, blue:0.48, alpha:1.00)
        static let purple = UIColor(red:0.53, green:0.26, blue:0.67, alpha:1.00)
    }
    
    struct Buttons {
        static let borderColor = UIColor(red:0.80, green:0.80, blue:0.80, alpha:1.00)
    }
}
