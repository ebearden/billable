//
//  DateHelper.swift
//  Billable
//
//  Created by Elvin Bearden on 5/31/16.
//  Copyright © 2016 Elvin Bearden. All rights reserved.
//

import UIKit

struct DateHelper {
    static let formatter = DateFormatter()
    static func dateString(_ date: Date) -> String {
        formatter.dateFormat = "MM-dd-yyyy"
        
        return formatter.string(from: date)
    }
    
    static func fullDateString(_ date: Date) -> String {
        formatter.dateFormat = "EEEE, MMM d"
        return formatter.string(from: date)
    }
}
