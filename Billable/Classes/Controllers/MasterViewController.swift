//
//  MasterViewController.swift
//  Billable
//
//  Created by Elvin Bearden on 5/17/16.
//  Copyright © 2016 Elvin Bearden. All rights reserved.
//

import UIKit

class MasterViewController: UITableViewController {
    var detailViewController: ClientDetailTableViewController?

    let viewModel = MasterViewModel()
    var emptyClientView: UIView?
}

// MARK: - View Lifecycle -
extension MasterViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.leftBarButtonItem = self.editButtonItem
        
        viewModel.refreshBlock = {
            self.tableView.reloadData()
        }
        
        navigationController?.navigationBar.barTintColor = Colors.Base.green
        navigationController?.navigationBar.tintColor = UIColor.white
        
        if let split = self.splitViewController {
            let controllers = split.viewControllers
            
            if let navController = controllers[controllers.count - 1] as? UINavigationController {
                self.detailViewController = navController.topViewController as? ClientDetailTableViewController
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if let splitVC = self.splitViewController {
            self.clearsSelectionOnViewWillAppear = splitVC.isCollapsed
        }
        
        super.viewWillAppear(animated)
        
        viewModel.refresh()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}

// MARK: - Segues -
extension MasterViewController {
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showDetail" {
            if let indexPath = self.tableView.indexPathForSelectedRow,
                let object = self.viewModel.dataSource?[indexPath.row],
                let navController = segue.destination as? UINavigationController,
                let controller = navController.topViewController as? ClientDetailTableViewController {
                
                controller.client = object
                controller.navigationItem.leftBarButtonItem = self.splitViewController?.displayModeButtonItem
                controller.navigationItem.leftItemsSupplementBackButton = true
            }
        }
        else if segue.identifier == "newClient" {
            if let navController = segue.destination as? UINavigationController,
                let controller = navController.topViewController as? AddEditClientViewController {
                
                navController.navigationBar.barTintColor = Colors.Base.green
                navController.navigationBar.tintColor = UIColor.white
                navController.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.white]
                
                controller.delegate = self
            }
        }
    }
}

extension MasterViewController: ClientUpdateDelegate {
    func clientUpdated() {
        tableView.reloadData()
    }
}


// MARK: - Table View -
extension MasterViewController {
    override func numberOfSections(in tableView: UITableView) -> Int {
        return viewModel.numberOfSections()
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.numberOfRowsInSection(section)
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return viewModel.cellForRow(tableView, indexPath: indexPath)
    }

    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }

    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            viewModel.delete(indexPath)
            
            tableView.beginUpdates()
            tableView.deleteRows(at: [indexPath], with: .automatic)
            tableView.endUpdates()
        }
    }
}

