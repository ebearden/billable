//
//  DateEntryQuickEntryViewController.swift
//  Billable
//
//  Created by Brian Bethke on 7/20/16.
//  Copyright © 2016 Elvin Bearden. All rights reserved.
//

import UIKit

class DateQuickEntryViewController: UIViewController, ModalPopover {
    var viewModel: DateEntryViewModel?
    
    @IBOutlet var entryTitleTextField: UITextField!
    @IBOutlet var selectCategoryButton: UIButton!
    @IBOutlet var cancelButton: UIButton!
    
    fileprivate var selectedCategory = BillableEntryCategory.general
    
    // MARK: - View Lifecycle -
    override func viewDidLoad() {
        super.viewDidLoad()
        
        addBlurToView(view)
        addTapGestureRecognizer(self, action: #selector(self.handleTap(_:)))
        
        populateFields()
        styleButtonBorder()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "selectCategory" {
            if let navController = segue.destination as? UINavigationController,
                let categoriesVC = navController.topViewController as? CategoriesViewController {
                
                navController.navigationBar.barTintColor = Colors.Base.green
                navController.navigationBar.tintColor = UIColor.white
                navController.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.white]
                
                categoriesVC.delegate = self
            }
        }
    }
}

// MARK: - CategoriesDelegate
extension DateQuickEntryViewController: CategoriesDelegate {
    func didSelectCategory(_ category: BillableEntryCategory) {
        selectCategoryButton.setTitle(category.name(), for: UIControlState())
        selectedCategory = category
    }
}

// MARK: - Actions
extension DateQuickEntryViewController {
    @IBAction func submitButtonPressed(_ sender: AnyObject) {
        var titleText = entryTitleTextField.text?.trimmingCharacters(in: CharacterSet.whitespaces)
        
        if titleText == "" {
            titleText = nil
        }
        
        viewModel?.updateBillableEntry(titleText, category: selectedCategory)
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func cancelButtonPressed(_ sender: AnyObject) {
        dismiss(animated: true, completion: nil)
    }

    func handleTap(_ sender: UITapGestureRecognizer) {
        if sender.state == .ended {
            dismiss(animated: true, completion: nil)
        }
    }
}

// MARK: - Private
extension DateQuickEntryViewController {
    fileprivate func populateFields() {
        guard let entry = viewModel?.selectedEntry else { return }
        
        selectCategoryButton.setTitle(entry.category.name(), for: UIControlState())
        selectedCategory = entry.category
        entryTitleTextField.text = entry.title
    }
    
    fileprivate func styleButtonBorder() {
        selectCategoryButton.layer.borderColor = Colors.Buttons.borderColor.cgColor
        cancelButton.layer.borderColor = Colors.Buttons.borderColor.cgColor
    }
}
