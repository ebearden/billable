//
//  ClientDescriptionViewController.swift
//  Billable
//
//  Created by Elvin Bearden on 7/23/16.
//  Copyright © 2016 Elvin Bearden. All rights reserved.
//

import UIKit

class ClientDescriptionViewController: UIViewController, ModalPopover {
    
    @IBOutlet weak var clientNameLabel: UILabel!
    @IBOutlet weak var textView: UITextView!
    
    var clientDescription: String = ""
    var clientName: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        textView.text = clientDescription
        clientNameLabel.text = clientName
        
        addBlurToView(view)
        addTapGestureRecognizer(self, action: #selector(self.handleTap(_:)))
    }
    
    func handleTap(_ sender: UITapGestureRecognizer) {
        if sender.state == .ended {
            dismiss(animated: true, completion: nil)
        }
    }
    
    @IBAction func doneButtonPressed(_ sender: AnyObject) {
        dismiss(animated: true, completion: nil)
    }
    
}
