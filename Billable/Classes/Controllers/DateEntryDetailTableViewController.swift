//
//  DateEntryDetailTableViewController.swift
//  Billable
//
//  Created by Elvin Bearden on 7/2/16.
//  Copyright © 2016 Elvin Bearden. All rights reserved.
//

import UIKit
// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l > r
  default:
    return rhs < lhs
  }
}


class DateEntryDetailTableViewController: UITableViewController {
    @IBOutlet weak var currentTimerLabel: UILabel!
    @IBOutlet weak var editButton: UIButton!
    @IBOutlet var timerButton: UIButton!
    
    var viewModel: DateEntryViewModel?
    var timer: Timer?
}

// MARK: - Lifecycle -
extension DateEntryDetailTableViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
        
        viewModel?.refreshBlock = { [weak self] in
            guard let weakSelf = self else { return }
            let selectedIndexPath = weakSelf.tableView.indexPathForSelectedRow
            weakSelf.tableView.reloadData()
            weakSelf.tableView.selectRow(at: selectedIndexPath, animated: false, scrollPosition: .none)
        }
    
        setupActionButton()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        selectFirstRow()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "quickEntry" {
            guard let viewModel = viewModel else { return }
            if let controller = segue.destination as? DateQuickEntryViewController {
                controller.navigationItem.leftBarButtonItem = self.splitViewController?.displayModeButtonItem
                controller.navigationItem.leftItemsSupplementBackButton = true
                
                controller.viewModel = viewModel
            }
        }
    }
}

// MARK: - TableView Methods -
extension DateEntryDetailTableViewController {
    override func numberOfSections(in tableView: UITableView) -> Int {
        return viewModel?.numberOfSections() ?? 0
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel?.numberOfRowsInSection(section) ?? 0
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return viewModel?.cellForRow(tableView, indexPath: indexPath) ?? UITableViewCell()
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        viewModel?.didSelectRow(indexPath)
        setupTimer()
        if let billable = viewModel?.dateEntry.billableEntries[indexPath.row] {
            currentTimerLabel.text = billable.currentTime()
            setupActionButton()
        }
    }
    
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            if let billable = viewModel?.dateEntry.billableEntries[indexPath.row] {
                if billable.active {
                    viewModel?.activeEntry = nil
                    timer?.invalidate()
                    
                }
                PersistanceManager.delete(billable) { (status) in
                    print(status)
                }
            }
            
            tableView.deleteRows(at: [indexPath], with: .fade)
        }
    }
}

// MARK: - Actions -
extension DateEntryDetailTableViewController {
    @IBAction func addButtonPressed(_ sender: AnyObject) {
        viewModel?.addBillableEntry(nil, category: .general)
        selectFirstRow()
        setupActionButton()
    }
    
    @IBAction func timerButtonPressed(_ sender: AnyObject) {
        if timerButton.titleLabel?.text == "Stop" {
            PersistanceManager.performTransaction {
                self.viewModel?.selectedEntry?.stop()
            }
            setupActionButton()
        }
        else {
            PersistanceManager.performTransaction({ 
                self.viewModel?.selectedEntry?.start()
            })
        }
    }
    
    @IBAction func editButtonPressed(_ sender: AnyObject) {
        guard let quickEditVC = storyboard?.instantiateViewController(withIdentifier: "quickEntry") as?
            DateQuickEntryViewController else { return }
        
        quickEditVC.modalPresentationStyle = .overFullScreen
        quickEditVC.modalTransitionStyle = .crossDissolve
        quickEditVC.navigationItem.leftBarButtonItem = self.splitViewController?.displayModeButtonItem
        quickEditVC.navigationItem.leftItemsSupplementBackButton = true
        
        quickEditVC.viewModel = viewModel
        
        present(quickEditVC, animated: true, completion: nil)
    }
    
    func timerUpdated() {
        if let billable = viewModel?.selectedEntry {
            guard billable.active else {
                timer?.invalidate()
                return
            }
            currentTimerLabel.text = billable.currentTime()
        }
        else {
            timer?.invalidate()
        }
    }
}


// MARK: - Private Methods -
extension DateEntryDetailTableViewController {
    fileprivate func selectFirstRow() {
        guard viewModel?.numberOfRowsInSection(0) > 0 else { return }
        let indexPath = IndexPath(row: 0, section: 0)
        viewModel?.updateSelectedEntry(indexPath)
        tableView.selectRow(
            at: indexPath,
            animated: true,
            scrollPosition: .top
        )
        setupTimer()
        if let billable = viewModel?.dateEntry.billableEntries[indexPath.row] {
            currentTimerLabel.text = billable.currentTime()
        }
    }
    
    fileprivate func setupActionButton() {
        if let selected = viewModel?.selectedEntry, selected.active {
            timerButton.setTitle("Stop", for: UIControlState())
            timerButton.isHidden = false
        }
        else if let selected = viewModel?.selectedEntry, !selected.active {
            timerButton.isHidden = true
        }
        else {
            timerButton.isHidden = true
        }
    }
    
    fileprivate func setupTimer() {
        timer = Timer.scheduledTimer(
            timeInterval: 1,
            target: self,
            selector: #selector(self.timerUpdated),
            userInfo: nil,
            repeats: true
        )
    }
}
