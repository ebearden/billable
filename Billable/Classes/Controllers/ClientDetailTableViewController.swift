//
//  ClientDetailTableViewController.swift
//  Billable
//
//  Created by Elvin Bearden on 5/17/16.
//  Copyright © 2016 Elvin Bearden. All rights reserved.
//

import UIKit
import RealmSwift

class ClientDetailTableViewController: UITableViewController {
    fileprivate var viewModel: ClientDetailViewModel?
    
    var client: Client? {
        didSet {
            self.viewModel = ClientDetailViewModel(client: self.client!)
            self.viewModel?.refreshBlock = { self.tableView.reloadData() }
        }
    }
}

// MARK: - Lifecycle -
extension ClientDetailTableViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupHeaderView()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showDateEntry" {
            if let indexPath = self.tableView.indexPathForSelectedRow {
                guard let object = viewModel?.client.dateEntries[indexPath.row] else { return }
                
                if let controller = segue.destination as? DateEntryDetailTableViewController {
                
                    let dateViewModel = DateEntryViewModel(dateEntry: object)
                    controller.viewModel = dateViewModel
                    
                    controller.navigationItem.leftBarButtonItem = self.splitViewController?.displayModeButtonItem
                    controller.navigationItem.leftItemsSupplementBackButton = true
                }
            }
        }
        else if segue.identifier == "editClient" {
            guard let client = client else { return }
            
            if let navController = segue.destination as? UINavigationController,
                let controller = navController.topViewController as? AddEditClientViewController {
                
                navController.navigationBar.barTintColor = Colors.Base.green
                navController.navigationBar.tintColor = UIColor.white
                navController.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.white]
                
                controller.viewModel = AddEditClientViewModel(client: client)
                controller.delegate = self
            }
        }
        else if segue.identifier == "clientDescription" {
            if let viewController = segue.destination as? ClientDescriptionViewController {
                if let description = client?.clientDescription, let name = client?.displayName {
                    viewController.clientDescription = description
                    viewController.clientName = name
                }
            }
        }
    }
}

// MARK: - TableView -
extension ClientDetailTableViewController {
    override func numberOfSections(in tableView: UITableView) -> Int {
        return viewModel?.numberOfSections() ?? 0
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel?.numberOfRowsInSection(section) ?? 0
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return viewModel?.cellForRow(tableView, indexPath: indexPath) as? ClientDetailTableViewCell ?? UITableViewCell()
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return viewModel?.titleForHeader(section)
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        viewModel?.didSelectRow(indexPath)
    }
}

// MARK: - ClientUpdateDelegate -
extension ClientDetailTableViewController: ClientUpdateDelegate {
    func clientUpdated() {
        setupHeaderView()
    }
}

// MARK: - Private -
extension ClientDetailTableViewController {
    fileprivate func setupHeaderView() {
        if let headerView = tableView.tableHeaderView as? ProfileHeaderView {
            headerView.initialsLabel.text = client?.clientInitials()
            headerView.nameLabel.text = client?.displayName
            headerView.companyLabel.text = client?.company
            headerView.detailsLabel.text = client?.clientDetails()
            headerView.descriptionTextView.text = client?.clientDescription
            
            if client?.clientDescription == "" {
                headerView.moreButton.isHidden = true
                headerView.descriptionTextView.isHidden = true
            }
            
            tableView.tableHeaderView?.frame.size = headerView.systemLayoutSizeFitting(
                headerView.frame.size, withHorizontalFittingPriority: 0, verticalFittingPriority: 0
            )
        }
    }
}

// MARK: - Actions -
extension ClientDetailTableViewController {
    @IBAction func shareButtonPressed(_ sender: AnyObject) {
        guard let filePath = viewModel?.exportToCSV() else { return }
        
        let activityViewController = UIActivityViewController(
            activityItems: [filePath],
            applicationActivities: nil
        )
        activityViewController.setValue("Export from Billable", forKey: "subject")
        
        if UI_USER_INTERFACE_IDIOM() == .pad {
            activityViewController.popoverPresentationController?.sourceView = view
        }
        navigationController?.present(activityViewController, animated: true, completion: nil)
    }
    
    @IBAction func addButtonPressed(_ sender: AnyObject) {
        viewModel?.addBillableEntry(nil, category: .general)
    }
    
}
