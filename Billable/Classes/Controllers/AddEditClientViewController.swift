//
//  AddEditClientViewController.swift
//  Billable
//
//  Created by Elvin Bearden on 6/4/16.
//  Copyright © 2016 Elvin Bearden. All rights reserved.
//

import UIKit

protocol ClientUpdateDelegate {
    func clientUpdated()
}

class AddEditClientViewController: UITableViewController {
    var viewModel = AddEditClientViewModel()
    var delegate: ClientUpdateDelegate?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupViewModel()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        viewModel.activeTextField?.becomeFirstResponder()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        viewModel.activeTextField?.resignFirstResponder()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}

// MARK: - Actions -
extension AddEditClientViewController {
    
    @IBAction func cancelPressed(_ sender: AnyObject) {
        navigationController?.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func savePressed(_ sender: AnyObject) {
        viewModel.save { (status) in
            self.delegate?.clientUpdated()
            self.navigationController?.dismiss(animated: true, completion: nil)
        }
    }
}

extension AddEditClientViewController: ViewModelDelegate {
    
    func presentViewController(_ viewController: UIViewController) {
        navigationController?.present(viewController, animated: true, completion: nil)
    }
}

// MARK: - TableView Delegates -
extension AddEditClientViewController {
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return viewModel.numberOfSections()
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.numberOfRowsInSection(section)
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return viewModel.cellForRow(tableView, indexPath: indexPath)
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        return viewModel.didSelectRow(indexPath)
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return viewModel.heightForRow(indexPath)
    }
}

// MARK: - Private -
extension AddEditClientViewController {
    
    fileprivate func setupViewModel() {
        viewModel.refreshBlock = { self.tableView.reloadData() }
        viewModel.delegate = self
    }
}
