//
//  CategoriesViewController.swift
//  Billable
//
//  Created by Brian Bethke on 7/23/16.
//  Copyright © 2016 Elvin Bearden. All rights reserved.
//

import UIKit

protocol CategoriesDelegate {
    func didSelectCategory(_ category: BillableEntryCategory)
}

class CategoriesViewController: UITableViewController {
    var delegate: CategoriesDelegate?
}

// MARK: - Actions
extension CategoriesViewController {
    
    @IBAction func didSelectCancel(_ sender: AnyObject) {
        dismiss(animated: true, completion: nil)
    }
}

// MARK: - UITableViewDelegate
extension CategoriesViewController {
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let category = BillableEntryCategory(rawValue: indexPath.row), let delegate = delegate {
            delegate.didSelectCategory(category)
            dismiss(animated: true, completion: nil)
        }
    }
}

// MARK: - UITableViewDataSource
extension CategoriesViewController {
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CategoryCell", for: indexPath)
        if let category = BillableEntryCategory(rawValue: indexPath.row) {
            cell.textLabel?.text = category.name()
        }
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return BillableEntryCategory.count
    }
}
