//
//  DateEntryViewModel.swift
//  Billable
//
//  Created by Elvin Bearden on 7/2/16.
//  Copyright © 2016 Elvin Bearden. All rights reserved.
//

import UIKit

class DateEntryViewModel: ViewModel {
    fileprivate let billableEntryCellId = "BillableEntryCell"
    
    var refreshBlock: ViewModelRefreshBlock?
    var dateEntry: DateEntry
    var selectedEntry: BillableEntry?
    var activeEntry: BillableEntry?
    
    init(dateEntry: DateEntry) {
        self.dateEntry = dateEntry
        self.selectedEntry = dateEntry.billableEntries.first
    }
    
    func numberOfSections() -> Int {
        return 1
    }
    
    func numberOfRowsInSection(_ section: Int) -> Int {
        return dateEntry.billableEntries.count
    }
    
    func cellForRow(_ tableView: UITableView, indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: billableEntryCellId) {
            let billable = dateEntry.billableEntries[indexPath.row]
            cell.textLabel?.text = billable.category.name()
            cell.detailTextLabel?.text = billable.title
            return cell
        }
        
        fatalError()
    }
    
    func didSelectRow(_ indexPath: IndexPath) {
        updateSelectedEntry(indexPath)
    }
    
    func updateSelectedEntry(_ indexPath: IndexPath) {
        selectedEntry = dateEntry.billableEntries[indexPath.row]
    }
    
    func addBillableEntry(_ title: String?, category: BillableEntryCategory?) {
        PersistanceManager.performTransaction {
            self.activeEntry?.stop()
            
            let billable = BillableEntry()
            billable.start()
            billable.title = title ?? "Entry \(self.dateEntry.billableEntries.count)"
            billable.category = category ?? .general
            self.dateEntry.billableEntries.insert(billable, at: 0)
            self.activeEntry = billable
            
            self.refreshBlock?()
        }
    }
    
    func updateBillableEntry(_ title: String?, category: BillableEntryCategory?) {
        guard let selectedEntry = selectedEntry else { return }
        
        PersistanceManager.performTransaction {
            selectedEntry.title = title
            selectedEntry.category = category ?? .general
            
            self.refreshBlock?()
        }
    }
}
