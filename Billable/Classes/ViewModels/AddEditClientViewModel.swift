//
//  AddEditClientViewModel.swift
//  Billable
//
//  Created by Elvin Bearden on 6/4/16.
//  Copyright © 2016 Elvin Bearden. All rights reserved.
//

import UIKit

class AddEditClientViewModel: NSObject, ViewModel {
    var activeTextField: UITextField?
    var refreshBlock: ViewModelRefreshBlock?
    var delegate: ViewModelDelegate?
    
    fileprivate let textFieldCellId = "AddEditClientTextFieldCell"
    fileprivate let textViewCellId = "AddEditClientTextViewCell"
    
    fileprivate var client: Client
    fileprivate var textFields = [UITextField]()
    fileprivate var textViews = [UITextView]()
    fileprivate var isEditing = false
    

    init(client: Client, isEditing: Bool = true) {
        self.client = client
        self.isEditing = isEditing
    }
    
    override init() {
        self.client = Client()
    }
    
    func save(_ completion: @escaping PersistanceManagerCompletion) {
        for field in textFields {
            if textFieldIsValid(field) {
                saveTextFieldData(field)
            }
            else {
                showError("Please enter at least one character")
                return
            }
        }
        
        for view in textViews {
            saveTextViewData(view)
        }
        
        PersistanceManager.add(client, update: isEditing) { status in
            completion(status)
        }
    }
    
    func numberOfSections() -> Int {
        return 1
    }
    
    func numberOfRowsInSection(_ section: Int) -> Int {
        return Client.supportedFields.count
    }
    
    func heightForRow(_ indexPath: IndexPath) -> CGFloat {
        let field = Client.supportedFields[indexPath.row]
        if field == SupportedFields.description {
            return 170
        }
        return 44
    }
    
    
    // TODO: There MUST be a better way to do this....
    func cellForRow(_ tableView: UITableView, indexPath: IndexPath) -> UITableViewCell {
        let field = Client.supportedFields[indexPath.row]
        switch field {
        case .firstName:
            if let cell = tableView.dequeueReusableCell(withIdentifier: textFieldCellId) as? AddEditClientTextFieldCell {
                cell.textField.placeholder = "First name"
                cell.textField.autocapitalizationType = .words
                activeTextField = cell.textField
                if isEditing {
                    cell.textField.text = client.firstName
                }
                
                cell.textField.tag = field.rawValue
                cell.textField.delegate = self
                textFields.append(cell.textField)
                
                return cell
            }
            break
            
        case .lastName:
            if let cell = tableView.dequeueReusableCell(withIdentifier: textFieldCellId) as? AddEditClientTextFieldCell {
                cell.textField.placeholder = "Last name"
                cell.textField.autocapitalizationType = .words
                if isEditing {
                    cell.textField.text = client.lastName
                }
                
                cell.textField.tag = field.rawValue
                cell.textField.delegate = self
                textFields.append(cell.textField)
                
                return cell
            }
            break
            
        case .company:
            if let cell = tableView.dequeueReusableCell(withIdentifier: textFieldCellId) as? AddEditClientTextFieldCell {
                cell.textField.placeholder = "Company"
                cell.textField.autocapitalizationType = .words
                cell.textField.autocorrectionType = .yes
                if isEditing {
                    cell.textField.text = client.company
                }
                
                cell.textField.tag = field.rawValue
                cell.textField.delegate = self
                textFields.append(cell.textField)
                
                return cell
            }
            break
            
        case .fileNumber:
            if let cell = tableView.dequeueReusableCell(withIdentifier: textFieldCellId) as? AddEditClientTextFieldCell {
                cell.textField.placeholder = "File #"
                cell.textField.autocapitalizationType = .words
                if isEditing {
                    cell.textField.text = client.fileNumber
                }
                
                cell.textField.tag = field.rawValue
                cell.textField.delegate = self
                textFields.append(cell.textField)
                
                return cell
            }
            break
            
        case .caseNumber:
            if let cell = tableView.dequeueReusableCell(withIdentifier: textFieldCellId) as? AddEditClientTextFieldCell {
                cell.textField.placeholder = "Case #"
                cell.textField.autocapitalizationType = .words
                if isEditing {
                    cell.textField.text = client.caseNumber
                }
                
                cell.textField.tag = field.rawValue
                cell.textField.delegate = self
                textFields.append(cell.textField)
                
                return cell
            }
            break
            
        case .description:
            if let cell = tableView.dequeueReusableCell(withIdentifier: textViewCellId) as? AddEditClientTextViewCell {
                if isEditing {
                    cell.textView.text = client.clientDescription
                }
                
                cell.textView.tag = field.rawValue
                textViews.append(cell.textView)
                
                return cell
            }
            break
        }
        
        fatalError()
    }
    
    func didSelectRow(_ indexPath: IndexPath) {}
    
    func saveTextFieldData(_ textField: UITextField) {
        switch textField.tag {
        case SupportedFields.firstName.rawValue:
            PersistanceManager.performTransaction {
                self.client.firstName = textField.text
            }
            break
            
        case SupportedFields.lastName.rawValue:
            PersistanceManager.performTransaction {
                self.client.lastName = textField.text
            }
            break
            
        case SupportedFields.company.rawValue:
            PersistanceManager.performTransaction {
                self.client.company = textField.text
            }
            break
            
        case SupportedFields.fileNumber.rawValue:
            PersistanceManager.performTransaction {
                self.client.fileNumber = textField.text
            }
            break
            
        case SupportedFields.caseNumber.rawValue:
            PersistanceManager.performTransaction {
                self.client.caseNumber = textField.text
            }
            break
            
        default:
            fatalError("Unsupported Field")
        }
    }
    
    fileprivate func saveTextViewData(_ textView: UITextView) {
        PersistanceManager.performTransaction { 
            self.client.clientDescription = textView.text
        }
    }
}

// MARK: - Private
extension AddEditClientViewModel {
    func showError(_ message: String) {
        let alertController = UIAlertController(
            title: "Error",
            message: message,
            preferredStyle: .alert
        )
        
        let cancelAction = UIAlertAction(title: "Okay", style: .cancel, handler: nil)
        alertController.addAction(cancelAction)
        
        delegate?.presentViewController(alertController)
    }
}

// MARK: - UITextField
extension AddEditClientViewModel: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        guard let thisTextField = activeTextField, thisTextField.tag < textFields.count - 1 else { return false }
        
        guard textFieldIsValid(textField) else {
            showError("Please enter at least one character")
            return false
        }
        
        if textField == activeTextField {
            activeTextField?.resignFirstResponder()
            activeTextField = textFields[thisTextField.tag + 1]
            activeTextField?.becomeFirstResponder()
        }
        
        return true
    }
    
    func textFieldIsValid(_ textField: UITextField) -> Bool {
        guard let textToValidate = textField.text else { return false }
        
        switch textField.tag {
        case SupportedFields.firstName.rawValue:
            return !textToValidate.isEmpty
            
        case SupportedFields.lastName.rawValue:
            return !textToValidate.isEmpty
            
        case SupportedFields.company.rawValue:
            return true
            
        case SupportedFields.fileNumber.rawValue:
            return true
            
        case SupportedFields.caseNumber.rawValue:
            return true
            
        default:
            fatalError("Unsupported Field")
        }
    }
}
