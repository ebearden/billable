//
//  MasterViewModel.swift
//  Billable
//
//  Created by Elvin Bearden on 5/17/16.
//  Copyright © 2016 Elvin Bearden. All rights reserved.
//

import UIKit
import RealmSwift

class MasterViewModel {
    var dataSource: Results<Client>?
    var refreshBlock: (() -> Void)?
    
    init() {
        refresh()
    }
    
    func delete(_ indexPath: IndexPath) {
        if let client = dataSource?[indexPath.row] {
            PersistanceManager.delete(client) { (status) in
                print(status)
            }
        }
    }
    
    func refresh() {
        PersistanceManager.get(Client.self, sortBy: "firstName", ascending: true) { status in
            switch status {
                
            case .success(let results):
                if let results = results as? Results<Client> {
                    self.dataSource = results
                }
                
            case .failure(let error):
                print(error)
                
            }
            
            self.refreshBlock?()
        }
    }
    
    func numberOfSections() -> Int {
        return 1
    }
    
    func numberOfRowsInSection(_ section: Int) -> Int {
        return dataSource?.count ?? 0
    }
    
    func cellForRow(_ tableView: UITableView, indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ClientCell", for: indexPath)
        if let client = dataSource?[indexPath.row] {
            cell.textLabel?.text = client.displayName
            cell.detailTextLabel?.text = client.company ?? ""
        }
        
        return cell
    }
    
    func isEmpty() -> Bool {
        guard let dataSource = dataSource else { return false }
        return dataSource.count < 1
    }
}
