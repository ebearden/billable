//
//  ClientDetailViewModel.swift
//  Billable
//
//  Created by Elvin Bearden on 5/17/16.
//  Copyright © 2016 Elvin Bearden. All rights reserved.
//

import UIKit

enum Section: Int {
    case profile
    case basic
}

struct ClientDetailViewModel: ViewModel {
    fileprivate let clientProfileCellId = "ClientDetailProfileTableViewCell"
    fileprivate let basicCellId = "BasicCell"
    fileprivate var dateEntry: DateEntry?
    
    var refreshBlock: ViewModelRefreshBlock?
    
    var client: Client
    
    init(client: Client) {
        self.client = client
        ClientDetailViewModel.addDateEntry(client) { dateEntry in
            self.dateEntry = dateEntry
        }
    }
    
    func numberOfSections() -> Int {
        return 1
    }
    
    func numberOfRowsInSection(_ section: Int) -> Int {
        switch section {
        default: return client.dateEntries.count
        }
    }
    
    func titleForHeader(_ section: Int) -> String {
        switch section {
        default: return "Billable Entries"
        }
    }
    
    func heightForRow(_ indexPath: IndexPath) -> CGFloat {
        switch indexPath.section {
        default: return 50
        }
    }
    
    func cellForRow(_ tableView: UITableView, indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section {
        default:
            if let cell = tableView.dequeueReusableCell(withIdentifier: basicCellId) as? ClientDetailTableViewCell {
                let dateEntry = client.dateEntries[indexPath.row]
                cell.dateEntry = dateEntry
                
                return cell
            }
        }
        
        return UITableViewCell()
    }
    
    static func addDateEntry(_ client: Client, completion: (DateEntry) -> Void) {
        PersistanceManager.performTransaction {
            var returnEntry = DateEntry()
            let shouldAddNewDate = !client.dateEntries.contains { entry in
                return returnEntry == entry
            }
            
            if shouldAddNewDate {
                client.dateEntries.append(returnEntry)
            }
            else if let dateEntry = client.dateEntries.first {
                returnEntry = dateEntry
            }
            
            completion(returnEntry)
            return
        }
    }
    
    func didSelectRow(_ indexPath: IndexPath) {
    }
    
    func exportToCSV() -> URL {
        return CSVExporter.export(client)
    }
    
    func addBillableEntry(_ title: String?, category: BillableEntryCategory?) {
        PersistanceManager.performTransaction {
            let billable = BillableEntry()
            billable.start()
            billable.category = category ?? .general
            
            if let dateEntry = self.dateEntry {
                billable.title = title ?? "Entry \(dateEntry.billableEntries.count)"
                dateEntry.billableEntries.insert(billable, at: 0)
            }
            
            self.refreshBlock?()
        }
    }
}
