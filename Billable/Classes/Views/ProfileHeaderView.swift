//
//  ProfileHeaderView.swift
//  Billable
//
//  Created by Elvin Bearden on 6/12/16.
//  Copyright © 2016 Elvin Bearden. All rights reserved.
//

import UIKit

class ProfileHeaderView: UIView {
    @IBOutlet weak var initialsLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var companyLabel: UILabel!
    @IBOutlet weak var detailsLabel: UILabel!
    @IBOutlet weak var descriptionTextView: UITextView!
    @IBOutlet weak var moreButton: UIButton!
    
    override func draw(_ rect: CGRect) {
        initialsLabel.layer.cornerRadius = initialsLabel.frame.width / 2
        initialsLabel.layer.masksToBounds = true
    }
    
    override func systemLayoutSizeFitting(_ targetSize: CGSize, withHorizontalFittingPriority horizontalFittingPriority: UILayoutPriority, verticalFittingPriority: UILayoutPriority) -> CGSize {
        if descriptionTextView.isHidden {
            moreButton.removeConstraints(moreButton.constraints)
            descriptionTextView.removeConstraints(descriptionTextView.constraints)
            return CGSize(width: targetSize.width, height: 105)
        }
        
        return CGSize(width: targetSize.width, height: 161)
    }
}
