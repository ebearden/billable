//
//  ClientDetailTableViewCell.swift
//  Billable
//
//  Created by Elvin Bearden on 6/15/16.
//  Copyright © 2016 Elvin Bearden. All rights reserved.
//

import UIKit

class ClientDetailTableViewCell: UITableViewCell {
    @IBOutlet weak var titleLabel: UILabel!
    
    var dateEntry: DateEntry? {
        didSet {
            if let dateEntry = dateEntry {
                titleLabel.text = DateHelper.fullDateString(dateEntry.date)
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}
