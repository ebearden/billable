//
//  AddEditClientTextViewCell.swift
//  Billable
//
//  Created by Elvin Bearden on 7/20/16.
//  Copyright © 2016 Elvin Bearden. All rights reserved.
//

import UIKit

class AddEditClientTextViewCell: UITableViewCell {
    @IBOutlet weak var textView: UITextView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

}
