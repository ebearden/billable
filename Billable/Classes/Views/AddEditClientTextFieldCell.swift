//
//  AddEditClientTextFieldCell.swift
//  Billable
//
//  Created by Elvin Bearden on 6/4/16.
//  Copyright © 2016 Elvin Bearden. All rights reserved.
//

import UIKit

class AddEditClientTextFieldCell: UITableViewCell {
    @IBOutlet weak var textField: UITextField!

    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(false, animated: animated)
    }

}
