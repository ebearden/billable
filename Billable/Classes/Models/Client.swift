//
//  Client.swift
//  Billable
//
//  Created by Elvin Bearden on 5/17/16.
//  Copyright © 2016 Elvin Bearden. All rights reserved.
//

import Foundation
import RealmSwift

enum SupportedFields: Int {
    case firstName, lastName, company, fileNumber, caseNumber, description
}

class Client: Object {
    static let supportedFields: [SupportedFields] = [
        .firstName, .lastName, .company, .fileNumber, .caseNumber, .description
    ]
    
    dynamic var identifier = UUID().uuidString
    dynamic var firstName: String?
    dynamic var lastName: String?
    dynamic var company: String?
    dynamic var fileNumber: String?
    dynamic var caseNumber: String?
    dynamic var clientDescription: String?
    
    var dateEntries = List<DateEntry>()
    
    override static func primaryKey() -> String? {
        return "identifier"
    }
}

extension Client {
    var displayName: String {
        if let firstName = firstName, let lastName = lastName {
            return "\(firstName) \(lastName)"
        }
        else if let firstName = firstName {
            return firstName
        }
        else if let lastName = lastName {
            return lastName
        }
        
        return ""
    }
    
    func clientInitials() -> String {
        var initials = ""
        
        if let firstName = firstName, let firstInitial = firstName.characters.first {
            initials.append(firstInitial)
        }
        
        if let lastName = lastName, let lastInitial = lastName.characters.first {
            initials.append(lastInitial)
        }
        
        return initials
    }
    
    func clientDetails() -> String {
        var detailStrings = [String]()
        
        if let caseNumber = caseNumber, !caseNumber.isEmpty {
            detailStrings.append("Case #: \(caseNumber)")
        }
        
        if let fileNumber = fileNumber, !fileNumber.isEmpty {
            detailStrings.append("File #: \(fileNumber)")
        }
        
        return detailStrings.joined(separator: " | ")
    }

}
