//
//  DateEntry.swift
//  Billable
//
//  Created by Elvin Bearden on 6/28/16.
//  Copyright © 2016 Elvin Bearden. All rights reserved.
//

import UIKit
import RealmSwift

class DateEntry: Object {
    dynamic var identifier = UUID().uuidString
    dynamic var date = Date()
    
    var billableEntries = List<BillableEntry>()
    
    override static func primaryKey() -> String? {
        return "identifier"
    }
    
    override func isEqual(_ object: Any?) -> Bool {
        guard let entry = object as? DateEntry else { return false }
        return date == entry.date
    }
}

func ==(lhs: DateEntry, rhs: DateEntry) -> Bool {
    return DateHelper.dateString(lhs.date) == DateHelper.dateString(rhs.date)
}
