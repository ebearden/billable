//
//  BillableEntry.swift
//  Billable
//
//  Created by Elvin Bearden on 5/21/16.
//  Copyright © 2016 Elvin Bearden. All rights reserved.
//

import UIKit
import RealmSwift

@objc enum BillableEntryCategory: Int {
    case general = 0, phoneCall, email, hearing, meeting, meetingOther, research, drafting
    
    func name() -> String {
        switch self {
            case .general: return "General"
            case .phoneCall: return "Phone Call"
            case .email: return "Email"
            case .hearing: return "Hearing"
            case .meeting: return "Meeting with Client"
            case .meetingOther: return "Meeting/Other"
            case .research: return "Research"
            case .drafting: return "Drafting"
        }
    }
    
    static var count: Int { return BillableEntryCategory.drafting.rawValue + 1 }
}

class BillableEntry: Object {
    dynamic var identifier = UUID().uuidString
    dynamic var date = Date()
    
    dynamic var hours: Int = 0
    dynamic var minutes: Int = 0
    dynamic var seconds: Int = 0
    dynamic var totalTime: Double = 0.0
    
    dynamic var title: String?
    dynamic var category = BillableEntryCategory.general
    dynamic var notes: String?
    
    dynamic var startTime: TimeInterval = 0
    dynamic var endTime: TimeInterval = 0
    
    dynamic var active = false
    
    
    override static func primaryKey() -> String? {
        return "identifier"
    }
    
    func start() {
        active = true
        startTime = Date.timeIntervalSinceReferenceDate
    }
    
    func stop() {
        active = false
        endTime = Date.timeIntervalSinceReferenceDate
        calculateTime()
        calculateTotalTime()
    }
    
    func calculateTime() {
        let elapsedTime = endTime - startTime
        let interval = Int(elapsedTime)
        seconds = interval % 60
        minutes = (interval / 60) % 60
        hours = (interval / 3600)
    }
    
    func currentTime() -> String {
        let elapsedTime: Int
        if endTime > 0 {
            elapsedTime = Int(endTime - startTime)
        }
        else {
            elapsedTime = Int(Date.timeIntervalSinceReferenceDate - startTime)
        }
        return String(format: "%02d:%02d:%02d", elapsedTime / 3600, (elapsedTime / 60 % 60), (elapsedTime % 60))
    }
    
    func calculateTotalTime() {
        if seconds > 0 {
            minutes += 1
        }
        
        if minutes > 59 {
            hours += 1
        }
        
        let mins = (minutes % 60)
        let minFraction = ceil(Double(mins) / 10)
        
        totalTime = Double(hours) + (minFraction / 10)
    }
}
