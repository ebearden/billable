//
//  PersistanceManager.swift
//  Billable
//
//  Created by Elvin Bearden on 5/25/16.
//  Copyright © 2016 Elvin Bearden. All rights reserved.
//

import RealmSwift

typealias PersistanceManagerCompletion = (DBStatus) -> Void

/**
 The status result for the performed database action
 
 - Success: `Success(values: Any?)`
 - Failure: `Failure(error: String?)`
 */
enum DBStatus {
    /**
     Returned if operation was successful
     
     - parameter items returned from the operation
     */
    case success(values: Any?)
    
    /**
     Returned if operation failed
    
     - parameter error: The error message
     */
    case failure(error: String?)
}

/// Manages the `Realm` database queries and writes.
class PersistanceManager {
    static var database: Realm {
        do {
            return try Realm()
        }
        catch let realmError {
            print(realmError)
        }
        
        fatalError("Realm cannot be initialized")
    }
}

// MARK: - Public -
extension PersistanceManager {
    /**
     Adds an item to the database, with an optional flag to signify an update.
     
     - parameter item:       An item that inherits from `Object` (a `Realm` model)
     - parameter update:     A flag that signifies whether or not this should be performed as an update
     - parameter completion: `(DBStatus) -> Void` completion block
     */
    static func add(_ item: Object, update: Bool = false, completion: PersistanceManagerCompletion) {
        database.beginWrite()
        
        database.add(item, update: update)
        
        do {
            try database.commitWrite()
            completion(.success(values: nil))
        }
        catch let error {
            completion(.failure(error: "\(error)"))
        }
    }
    
    static func delete(_ item: Object, completion: PersistanceManagerCompletion) {
        do {
            try database.write {
                database.delete(item)
            }
            completion(.success(values: nil))
        }
        catch let error {
            completion(.failure(error: "\(error)"))
        }
    }
    
    
    /**
     Get all objects of `type`
     
     - parameter type:       The type of object to retrieve, must be subclass of `Object` (a `Realm` model)
     - parameter completion: `(DBStatus) -> Void` completion block
     */
    static func get<T: Object>(_ type: T.Type, sortBy: String?, ascending: Bool, completion: PersistanceManagerCompletion) {
        if let sortBy = sortBy {
            let results = database.objects(type).sorted(byProperty: sortBy, ascending: ascending)
            completion(.success(values: results))
        }
        else {
            let results = database.objects(type)
            completion(.success(values: results))
        }
    }
    
    
    static func performTransaction(_ transaction: () -> Void) {
        do {
            database.beginWrite()
            
            transaction()

            try database.commitWrite()
        }
        catch let error {
            print(error)
        }

    }
}
