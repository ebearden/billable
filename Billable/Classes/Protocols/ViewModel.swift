//
//  ViewModel.swift
//  Billable
//
//  Created by Elvin Bearden on 6/4/16.
//  Copyright © 2016 Elvin Bearden. All rights reserved.
//

import UIKit

typealias ViewModelRefreshBlock = (() -> Void)

protocol ViewModel {
    var refreshBlock: ViewModelRefreshBlock? { get set }
    
    func numberOfSections() -> Int
    func numberOfRowsInSection(_ section: Int) -> Int
    
    func didSelectRow(_ indexPath: IndexPath)
    func cellForRow(_ tableView: UITableView, indexPath: IndexPath) -> UITableViewCell
}
