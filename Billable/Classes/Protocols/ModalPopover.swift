//
//  ModalPopover.swift
//  Billable
//
//  Created by Elvin Bearden on 7/23/16.
//  Copyright © 2016 Elvin Bearden. All rights reserved.
//

import UIKit

protocol ModalPopover {
    func addBlurToView(_ view: UIView)
    func addTapGestureRecognizer(_ viewController: UIViewController, action: Selector)
    func handleTap(_ sender: UITapGestureRecognizer)
}

extension ModalPopover {
    func addBlurToView(_ view: UIView) {
        let blurEffect = UIBlurEffect(style: .dark)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.frame = view.bounds
        blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight] // for supporting device rotation
        view.addSubview(blurEffectView)
        view.sendSubview(toBack: blurEffectView)
    }
    
    func addTapGestureRecognizer(_ viewController: UIViewController, action: Selector) {
        let tap = UITapGestureRecognizer(target: viewController, action: action)
        tap.numberOfTapsRequired = 1
        viewController.view.addGestureRecognizer(tap)
    }
}
