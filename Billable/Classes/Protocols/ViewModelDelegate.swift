//
//  ViewModelDelegate.swift
//  Billable
//
//  Created by Elvin Bearden on 7/6/16.
//  Copyright © 2016 Elvin Bearden. All rights reserved.
//

import UIKit

protocol ViewModelDelegate {
    func presentViewController(_ viewController: UIViewController)
}
