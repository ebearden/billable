//
//  BillableEntryTests.swift
//  Billable
//
//  Created by Elvin Bearden on 5/21/16.
//  Copyright © 2016 Elvin Bearden. All rights reserved.
//

import XCTest
@testable import Billable

class BillableEntryTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    func testCase1() {
        calculateTotalTime((4, 10, 7), output: 4.2)
    }
    
    func testCase2() {
        calculateTotalTime((2, 50, 6), output: 2.9)
    }
    
    func testCase3() {
        calculateTotalTime((0, 10, 6), output: 0.2)
    }
    
    func testCase4() {
        calculateTotalTime((20, 59, 59), output: 21.0)
    }
    
    func calculateTotalTime(_ input: (h: Int, m: Int, s: Int), output: Double) {
        let billable = BillableEntry()
        billable.hours = input.h
        billable.minutes = input.m
        billable.seconds = input.s
        
        billable.calculateTotalTime()
        XCTAssertEqual(billable.totalTime, output)
    }
    
}
